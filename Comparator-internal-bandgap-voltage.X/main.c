/* 
 * File:   main.c
 * Author: pab
 *
 * Created on July 2, 2022, 5:52 PM
 */

#include <stdio.h>
#include <stdlib.h>

#include "mc_spec_files/system.h"

 

/*
 * 
 */
int main(int argc, char** argv) {
    
    
    //initialize system and its peripherals
    //check file mc_spec_files/system.h to change configuration
    System_Initialize();

    while(1)
    {
        //turn on LED if comparator is high
        if(GetComparatorOutputState())
        {
            LED_FullOn();
        }
        //turn off LED if comparator is low
        else
        {
            LED_FullOff();
        }
    }
    
    return 1;
}

