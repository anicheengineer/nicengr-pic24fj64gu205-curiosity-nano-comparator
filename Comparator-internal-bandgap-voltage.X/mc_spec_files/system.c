#include <p24FJ64GU205.h>

#include "system.h"


/*
 *********************************************************
 *                Configuration Bits                     *
 ********************************************************* 
 */
//File to define PIC microcontroller configuration

// Configuration bits: selected in the GUI

// FSEC
#pragma config BWRP = OFF    //Boot Segment Write-Protect bit->Boot Segment may be written
#pragma config BSS = DISABLED    //Boot Segment Code-Protect Level bits->No Protection (other than BWRP)
#pragma config BSEN = OFF    //Boot Segment Control bit->No Boot Segment
#pragma config GWRP = OFF    //General Segment Write-Protect bit->General Segment may be written
#pragma config GSS = DISABLED    //General Segment Code-Protect Level bits->No Protection (other than GWRP)
#pragma config CWRP = OFF    //Configuration Segment Write-Protect bit->Configuration Segment may be written
#pragma config CSS = DISABLED    //Configuration Segment Code-Protect Level bits->No Protection (other than CWRP)
#pragma config AIVTDIS = OFF    //Alternate Interrupt Vector Table bit->Disabled AIVT

// FBSLIM
#pragma config BSLIM = 8191    //Boot Segment Flash Page Address Limit bits->8191

// FOSCSEL
#pragma config FNOSC = FRC    //Oscillator Source Selection->FRC
#pragma config PLLMODE = PLL96DIV2    //PLL Mode Selection->96 MHz PLL. Oscillator input is divided by 2 (8 MHz input)
#pragma config IESO = OFF    //Two-speed Oscillator Start-up Enable bit->Start up with user-selected oscillator source

// FOSC
#pragma config POSCMD = NONE    //Primary Oscillator Mode Select bits->Primary Oscillator disabled
#pragma config OSCIOFCN = ON    //OSC2 Pin Function bit->OSC2 is general purpose digital I/O pin
#pragma config SOSCSEL = ON    //SOSC Selection Configuration bits->SOSC is used in crystal (SOSCI/SOSCO) mode
#pragma config PLLSS = PLL_FRC    //PLL Secondary Selection Configuration bit->PLL is fed by the on-chip Fast RC (FRC) oscillator
#pragma config IOL1WAY = ON    //Peripheral pin select configuration bit->Allow only one reconfiguration
#pragma config FCKSM = CSECMD    //Clock Switching Mode bits->Clock switching is enabled,Fail-safe Clock Monitor is disabled

// FWDT
#pragma config WDTPS = PS32768    //Watchdog Timer Postscaler bits->1:32768
#pragma config FWPSA = PR128    //Watchdog Timer Prescaler bit->1:128
#pragma config FWDTEN = OFF    //Watchdog Timer Enable bits->WDT and SWDTEN disabled
#pragma config WINDIS = OFF    //Watchdog Timer Window Enable bit->Watchdog Timer in Non-Window mode
#pragma config WDTWIN = WIN25    //Watchdog Timer Window Select bits->WDT Window is 25% of WDT period
#pragma config WDTCMX = WDTCLK    //WDT MUX Source Select bits->WDT clock source is determined by the WDTCLK Configuration bits
#pragma config WDTCLK = LPRC    //WDT Clock Source Select bits->WDT uses LPRC

// FPOR
#pragma config BOREN = ON    //Brown Out Enable bit->Brown-out Reset is Enabled 
#pragma config LPREGEN = OFF    //Low power regulator control->Low Voltage and Low Power Regulator are not available
#pragma config LPBOREN = ENABLE    //Downside Voltage Protection Enable bit->Low Power BOR is enabled and active when main BOR is inactive

// FICD
#pragma config ICS = PGD3    //ICD Communication Channel Select bits->Communicate on PGEC3 and PGED3
#pragma config JTAGEN = OFF    //JTAG Enable bit->JTAG is disabled

// FDMTIVTL
#pragma config DMTIVTL = 0    //Deadman Timer Interval Low Word->0

// FDMTIVTH
#pragma config DMTIVTH = 0    //Deadman Timer Interval High Word->0

// FDMTCNTL
#pragma config DMTCNTL = 0    //Deadman Timer Instruction Count Low Word->0

// FDMTCNTH
#pragma config DMTCNTH = 0    //Deadman Timer Instruction Count High Word->0

// FMDT
#pragma config DMTDIS = OFF    //Deadman Timer Enable Bit->Dead Man Timer is Disabled and can be enabled by software

// FDEVOPT1
#pragma config ALTCMP1 = DISABLE    //Alternate Comparator 1 Input Enable bit->C1INC is on RB13 and C3INC is on RA0 
#pragma config TMPRPIN = OFF    //Tamper Pin Enable bit->TMPRN pin function is disabled
#pragma config SOSCHP = ON    //SOSC High Power Enable bit (valid only when SOSCSEL = 1->Enable SOSC high power mode (default)
#pragma config ALTI2C1 = ALTI2C1_OFF    //Alternate I2C pin Location->I2C1 Pin mapped to SDA1/SCL1 pins
#pragma config ALTCMP2 = DISABLE    //Alternate Comparator 2 Input Enable bit->C2INC is on RA4 and C2IND is on RB4
#pragma config SMB3EN = SMBUS3    //SM Bus Enable->SMBus 3.0 input levels


#define PIN_DIR_INPUT  1
#define PIN_DIR_OUTPUT 0


/*
 *********************************************************
 *                       LED Setup                       *
 ********************************************************* 
 */

//led pin setting
#define LED_STATE               LATCbits.LATC1                  
#define LED_PIN_DIRECTION       TRISCbits.TRISC1
#define LED_ON                  0 //led turns on when driven to GND
#define LED_OFF                 1


static void LED_Initialize(void)
{
    LED_PIN_DIRECTION = PIN_DIR_OUTPUT;
    LED_STATE = LED_OFF;
}

void LED_FullOn(void)
{
    LED_STATE = LED_ON;
}

void LED_FullOff(void)
{
    LED_STATE = LED_OFF;
}

/*
 *********************************************************
 *                       Comparator Setup                *
 ********************************************************* 
 */

#define COMPARATOR_REG          CM1CON

#define COMPARATOR_VREF_REG    CVRCON

            
#define COMPARATOR_PLUS_PIN_DIRECTION       TRISBbits.TRISB3

//
// Important Note. 
// For analog input pin of microcontroller, 
// the external GND pin of the microcontroller must be tied to 
// GND of the analog circuit from which input originates. 
// This is done to give a reference to analog pin of the microcontroller.
//
static void Comparator_Initialize(void)
{
    //bandgap voltage used for comparison is about 1.2V
    //C1INA is connected to pin RB3 on PIC24FJ64GU205
    
    //set noninverting input pin of comparator as input
    COMPARATOR_PLUS_PIN_DIRECTION = PIN_DIR_INPUT;
    
    //set voltage reference in CVRCON register
    COMPARATOR_VREF_REG = 0b0;
    
    CVRCONbits.CVREFP = 0; //don't care, cref is not 1 in comparator, bit 10
    CVRCONbits.CVREFM = 0b00; //Since comparator cch is 11, it is valid.
                            //band gap voltage is provided as input to comparator. bits 8-9
    CVRCONbits.CVREN = 0; // CVREF circuit is powered down, not in use, bit 7
    CVRCONbits.CVROE = 0; //Comparator VREF Output is not external, bit 6
    
    CVRCONbits.CVRSS = 0; //Comparator reference source, CVRSRC = AVDD ? AVSS, bit 5
    
    CVRCONbits.CVR = 16; //comparator value selection is 16 out of 32.  CVREF = (CVREF-) + (CVR[4:0]/32) � (CVREF+ ? CVREF-) 
    
    ANCFGbits.VBGEN2 = 1; //enable band gap reference for comparator
            
    //use bandgap as input to comparators
    //CVREF voltage level is disconnected from the CVREF pin
    //trigger/event generation is disabled
    
    //set comparator
    COMPARATOR_REG = 0b0;
    
    CM1CONbits.CON = 1; //enable comparator, bit 15
    CM1CONbits.COE = 0; //disable comparator external output, bit 14
    CM1CONbits.CPOL = 0; //output is not inverted, bit 13
    //CM1CONbits.CEVT is an output indicating compare event occurred, bit 12
    //CM1CONbits.COUT is an output indicating status of comparator output, bit 11 
    CM1CONbits.EVPOL = 0b00; //disable trigger/event/interrupt generation, bits 9-10
    CM1CONbits.CREF = 0; //C1INA pin connects to noninverting + input of comparator, bit 9
    //bits 2 and 3 are reserved
    CM1CONbits.CCH = 0b11; //inverting - input of comparator internal selectable reference voltage specified by the
                          //CVREFM[1:0] bits in the CVRCON register

    
    //Hysteresis is not enabled
    //Comparator not in sync with timer 1
    
    //Positive edge interrupt disabled
    //Negative edge interrupt disabled
    
}

#define COMPARATOR_OUTPUT_REG   CM1CONbits.COUT

bool GetComparatorOutputState(void)
{
    bool state = (COMPARATOR_OUTPUT_REG == 1) ? true : false;
    //CM1CONbits.CEVT = 0;//clear comparator Event bit
    return state;
}

/*
 *********************************************************
 *           Final Function for System Init              *
 ********************************************************* 
 */

void System_Initialize(void)
{
    LED_Initialize();
    Comparator_Initialize();
}

