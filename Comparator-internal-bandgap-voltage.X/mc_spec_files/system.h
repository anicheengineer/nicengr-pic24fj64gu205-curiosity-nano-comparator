/*

  @File Name:
    system.h

  @Summary:
    This is the system.h file for PIC16 / PIC24 / dsPIC33 / PIC32MM MCUs

  @Description:
    This header file provides implementations for generic 16-bit,PIC microcontrollers.
*/


#include "xc.h"
#include "stdint.h"
#include <stdbool.h>


#ifndef SYSTEM_H
#define	SYSTEM_H

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Initializes the device to the default states
 * @Example
    SYSTEM_Initialize(void);
 */
void System_Initialize(void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
   Makes LED glow at full brightness
 * @Example
    LED_FullOn(void);
 */
void LED_FullOn(void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
   Makes LED dark and off.
 * @Example
    LED_FullOff(void);
 */
void LED_FullOff(void);


/**
 * @Param
    none
 * @Returns
    bool
 * @Description
    returns state of comparator output.
 * @Example
    GetComparatorOutputState(void);
 */
bool GetComparatorOutputState(void);


#endif	/* SYSTEM_H */
/**
 End of File
*/