# pic24fj64gu205 curiosity nano comparator

This is a git project for containing projects that show how to use the comparator of pic24fj64gu205 curiosity nano devkit.

Each project folder has an application demo note that explains how to set up and use the application circuit and project code.

Projects

- Comparator-internal-bandgap-voltage.X shows how to compare if an external voltage is more than internal band gap voltage of 1.2 volts.

